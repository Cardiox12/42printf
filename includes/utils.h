/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <bbellavi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/17 19:49:22 by bbellavi          #+#    #+#             */
/*   Updated: 2020/02/13 10:44:40 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

# include <stddef.h>
# include "parser.h"

void	ft_bzero(void *s, size_t n);
size_t	ft_strlen(const char *s);
char	*ft_strchr(const char *s, int c);
int		ft_atoi(char *str);
int		ft_isdigit(unsigned char c);
int		ft_issymbol(char c);
int		ft_abs(int number);
char	*ft_strchrset(const char *s, const char *charset);
int		ft_is_negative(t_state *state);
int		ft_is_zero(t_state *state);

#endif
