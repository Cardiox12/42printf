# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bbellavi <bbellavi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/11/16 05:01:05 by bbellavi          #+#    #+#              #
#    Updated: 2020/02/13 17:52:25 by bbellavi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

COLOR_NC			= \e[0m
COLOR_LIGHT_GREEN	= \e[1;32m

CC				= gcc
FLAGS			=  -g -Wall -Werror -Wextra
INC_DIR 		= includes

BUF_API_DIR		= buffer
PARSE_API_DIR	= parser
UTILS_DIR		= utils
ROUTER_DIR		= router

HEADERS			= $(INC_DIR)/buffer.h
HEADERS			+= $(INC_DIR)/ft_printf.h
HEADERS			+= $(INC_DIR)/parser.h
HEADERS			+= $(INC_DIR)/router.h
HEADERS			+= $(INC_DIR)/utils.h

SRCS			= ft_printf.c
SRCS			+= $(PARSE_API_DIR)/parse.c
SRCS			+= $(PARSE_API_DIR)/init_state.c
SRCS			+= $(PARSE_API_DIR)/set_flags.c
SRCS			+= $(PARSE_API_DIR)/do_conv.c
SRCS			+= $(PARSE_API_DIR)/set_conv.c
SRCS			+= $(PARSE_API_DIR)/get_conv_length.c
SRCS			+= $(PARSE_API_DIR)/state_active.c
SRCS			+= $(PARSE_API_DIR)/parse_utils_1.c
SRCS			+= $(PARSE_API_DIR)/parse_utils_2.c
SRCS			+= $(PARSE_API_DIR)/pass.c
SRCS			+= $(BUF_API_DIR)/flush_buffer.c
SRCS			+= $(BUF_API_DIR)/init_buffer.c
SRCS			+= $(BUF_API_DIR)/show_buffer.c
SRCS			+= $(BUF_API_DIR)/append_buffer.c
SRCS			+= $(BUF_API_DIR)/append_string_buffer.c
SRCS			+= $(BUF_API_DIR)/append_nbytes_buffer.c
SRCS			+= $(BUF_API_DIR)/is_full.c
SRCS			+= $(UTILS_DIR)/ft_bzero.c
SRCS			+= $(UTILS_DIR)/ft_atoi.c
SRCS			+= $(UTILS_DIR)/ft_strlen.c
SRCS			+= $(UTILS_DIR)/ft_strchr.c
SRCS			+= $(UTILS_DIR)/ft_isdigit.c
SRCS			+= $(UTILS_DIR)/ft_issymbol.c
SRCS			+= $(UTILS_DIR)/ft_abs.c
SRCS			+= $(UTILS_DIR)/ft_strchrset.c
SRCS			+= $(UTILS_DIR)/ft_is_negative.c
SRCS			+= $(UTILS_DIR)/ft_is_zero.c
SRCS			+= $(ROUTER_DIR)/ptoa.c
SRCS			+= $(ROUTER_DIR)/itoa.c
SRCS			+= $(ROUTER_DIR)/itohl.c
SRCS			+= $(ROUTER_DIR)/itohu.c
SRCS			+= $(ROUTER_DIR)/utoa.c
SRCS			+= $(ROUTER_DIR)/make_padding.c
SRCS			+= $(ROUTER_DIR)/make_precision.c
SRCS			+= $(ROUTER_DIR)/make_both.c
OBJS			= $(SRCS:%.c=%.o)
NAME			= libftprintf.a
SUBMAKE			= make -C
.PHONY: all clean fclean re


all: $(NAME)

$(NAME): $(OBJS)
	@ar -rcs $@ $?

%.o: %.c
	@printf "$(COLOR_LIGHT_GREEN)Making$(COLOR_NC) -> $(?:.c=)\n"
	@$(CC) $(FLAGS) -o $@ -c $? -I$(INC_DIR)

clean:
	@rm -rf $(OBJS)

fclean: clean
	@printf ">>> $(COLOR_LIGHT_GREEN)Cleaning printf$(COLOR_NC) <<<\n"
	@rm -rf $(NAME)
	@rm -rf main
	@rm -rf main.dSYM

re: fclean $(NAME)